#!/bin/bash

set +ex

# Update submodules for the fossils db.
git submodule update --init --recursive

# Update the private git LFS repo.
pushd external/radv_fossils
git lfs pull
popd

if [ ! -d external/mesa ]; then
    # Clone the mesa repo.
    git clone https://gitlab.freedesktop.org/mesa/mesa.git external/mesa

    # Set require configuration.
    pushd external/mesa
    git config user.email "vk-cts-image@no-op"
    git config user.name "vk-cts-image"
    popd
fi
