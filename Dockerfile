FROM debian:testing

# Update and install the build software
RUN apt-get update

RUN apt-get dist-upgrade -y

# TODO: Try to reduce the number of installed packages.
RUN apt-get install -y \
        bison \
        cmake \
        flex \
        g++ \
        gcc \
        gettext \
        git \
        libelf-dev \
        libexpat1-dev \
        libgles2-mesa-dev \
        libunwind8 \
        libvulkan-dev \
        libvulkan1 \
        libx11-xcb-dev \
        libxcb-ewmh-dev \
        libxcb-ewmh2 \
        libxcb-keysyms1 \
        libxcb-keysyms1-dev \
        libxcb-randr0 \
        libxcb-xfixes0 \
        libxdamage1 \
        libxfixes3 \
        libxkbcommon-dev \
        libxkbcommon0 \
        libxrandr-dev \
        libxrandr2 \
        libxrender-dev \
        libxrender1 \
        libxshmfence-dev \
        libxxf86vm1 \
        meson \
        ninja-build \
        pkg-config \
        python \
        python3-attr \
        python3-distutils \
        python3-mako \
        wget

# Dependencies where we want a specific version.
ARG XCB_RELEASES=https://xcb.freedesktop.org/dist
ARG XCBPROTO_VERSION=xcb-proto-1.13
ARG LIBXCB_VERSION=libxcb-1.13

RUN wget $XCB_RELEASES/$XCBPROTO_VERSION.tar.bz2
RUN tar -xvf $XCBPROTO_VERSION.tar.bz2 && rm $XCBPROTO_VERSION.tar.bz2
RUN cd $XCBPROTO_VERSION; ./configure; make -j4 install; cd ..
RUN rm -rf $XCBPROTO_VERSION

RUN wget $XCB_RELEASES/$LIBXCB_VERSION.tar.bz2
RUN tar -xvf $LIBXCB_VERSION.tar.bz2 && rm $LIBXCB_VERSION.tar.bz2
RUN cd $LIBXCB_VERSION; ./configure; make -j4 install; cd ..
RUN rm -rf $LIBXCB_VERSION

# Copy build scripts.
COPY image/build-*.sh /root/

# Build LLVM.
RUN ./root/build-llvm.sh

# Build libdrm.
RUN ./root/build-drm.sh

# Build VK-GL-CTS.
RUN ./root/build-deqp-vk.sh

# Build parallel-deqp-runner
RUN ./root/build-deqp-runner.sh

# Build Fossilize.
RUN ./root/build-fossilize.sh

# Build shader-db.
RUN ./root/build-shader-db.sh

# Remove build scripts
RUN rm ./root/build-*.sh

# TODO: Purge useless pacakges

RUN apt-get autoremove -y --purge
