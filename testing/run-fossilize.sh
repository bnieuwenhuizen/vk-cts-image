#!/bin/bash

set -ex

RESULTS_DIR="/mnt/results"

run_fossilize()
{
    output_file=$1

    /mnt/testing/build-mesa.sh

    export LD_LIBRARY_PATH=/mnt/mesa/install/lib/:/usr/local/lib
    export VK_ICD_FILENAMES=/mnt/mesa/install/share/vulkan/icd.d/"$VK_DRIVER"_icd.x86_64.json

    if [ -n "$NUM_THREADS" ]; then
        FOSSILIZE_PARALLEL="--num-threads $NUM_THREADS"
    fi

    /usr/local/bin/fossil_replay.sh /mnt/radv_fossils/fossils \
        $output_file $FOSSILIZE_PARALLEL
}

# Run Fossilize against the source remote/branch.
dev_db="$RESULTS_DIR/fossilize-$MESA_SOURCE_BRANCH.csv"
run_fossilize $dev_db

if [ $FOSSILIZE_REPORT_RESULTS -eq 1 ]; then
    # If we want to report the Fossilize results, build the origin/master
    # branch. This assumes that the source branch has been rebased first.
    export MESA_SOURCE_REMOTE=origin
    export MESA_SOURCE_BRANCH=master
    export MESA_REBASE_BRANCH=0

    baseline_db="$RESULTS_DIR/fossilize-master.csv"
    run_fossilize $baseline_db

    # Report the results.
    /usr/local/bin/radv-report-fossil.py $baseline_db $dev_db
fi
