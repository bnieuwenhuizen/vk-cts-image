#!/bin/bash

set -ex

SHADER_DB_VERSION="9a8d5bb"

git clone https://gitlab.freedesktop.org/mesa/shader-db.git /shader-db
pushd /shader-db
git checkout $SHADER_DB_VERSION
cp fossil_replay.sh /usr/local/bin
cp radv-report-fossil.py /usr/local/bin
popd
rm -rf /shader-db
